import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { VolunteerComponent } from './volunteer/volunteer.component';
import { VolunteerListComponent } from './volunteer-list/volunteer-list.component';
import { VolunteerItemComponent } from './volunteer-item/volunteer-item.component';
import { VolunteerMapComponent } from './volunteer-map/volunteer-map.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GiveComponent } from './give/give.component';
import { MywayComponent } from './myway/myway.component';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from'angular2-google-maps/core';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { MyDatePickerModule } from 'mydatepicker';
import {ShareButtonsModule} from "ng2-sharebuttons";
import { LogInComponent } from './log-in/log-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';

const appRoutes: Routes = [
  {
    path:"volunteer",
    component:VolunteerComponent,
    children: [
      {path:'', redirectTo: 'map', pathMatch: 'full'},
      {path:"map", component:VolunteerMapComponent },
      {path:"list", component:VolunteerListComponent },
      {path:"map/:id", component:VolunteerItemComponent },
      {path:"list/:id", component:VolunteerItemComponent }
    ],
    data: {
      title: "Volunteer"
    }},
  {
    path:"dashboard",
    component: DashboardComponent,
    data: {
      title: "Dashboard"
    }
  },
  {
    path:"give",
    component: GiveComponent,
    data: {
      title: "Give"
    }
  },
  {
    path:"myway",
    component: MywayComponent,
    data: {
      title: "MyWay"
    }
  },
  {
    path:"",
    component: LogInComponent,
    data: {
      title: "Log In"
    }
  },
   {
    path:"signup",
    component: SignUpComponent,
    data: {
      title: "Sign Up"
    }
  }

];

@NgModule({
  declarations: [
    AppComponent,
    VolunteerComponent,
    VolunteerListComponent,
    VolunteerItemComponent,
    VolunteerMapComponent,
    DashboardComponent,
    GiveComponent,
    MywayComponent,
    LogInComponent,
    SignUpComponent
  ],
  imports: [
    MyDatePickerModule,
    BrowserModule,
    FormsModule,
    CommonModule,
    HttpModule,Ng2Bs3ModalModule,
    ShareButtonsModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC0ChX64h7schNW2YkLeNbZCaeE5T6HMYY'
    }),
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule { }
