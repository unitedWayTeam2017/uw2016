import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params } from '@angular/router';
import { AppService } from '../app.service';
import { VolunteerService } from '../volunteer.service';
import {Location} from '@angular/common';
import { Event, EventVolunteer } from '../volunteer/event';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';
import {IMyOptions} from 'mydatepicker';

@Component({
  selector: 'app-volunteer-item',
  templateUrl: './volunteer-item.component.html',
  styleUrls: ['./volunteer-item.component.css'],
  providers: [ AppService, VolunteerService ]
})
export class VolunteerItemComponent implements OnInit {
  private myDatePickerOptions: IMyOptions = {
      dateFormat: 'mm-dd-yyyy'
  };
  event: Event[]; // new Event(1, "TEST UNITED WAY", "3/19/2017", "11:00", "14:00", "This is a great event to come out and show support.", "625 North Orange Street", null, "Wilmington", "DE", "19801", -75.551465, 39.743089, null);
  eventVols: EventVolunteer[];
  public editMode = false;
  public editText = 'Edit Mode';
  public id = 0;
  public name;
  public email;
  public dt;
  public sTime;
  public sHour;
  public sMinute;
  public eTime;
  public eHour;
  public eMinute;
  public isAdmin;
  constructor(private route: ActivatedRoute,private appService: AppService, private volServ: VolunteerService, private location: Location) { }

  ngOnInit() {
//    this.route.params
//      .switchMap((params: Params) => this.volServ.getEvent(+params['id']))
//      .subscribe(event =>this.event = event);

      this.isAdmin = this.appService.isUserAdmin();

      this.route.params
        .subscribe(params => this.id = params['id']);

      this.appService.getEvent(this.id)
        .subscribe(event => {this.event = event;
          this.dt = event[0].startDate.getFullYear() + "-" + (event[0].startDate.getMonth()+1) + "-" + event[0].startDate.getDate();
          this.sHour = event[0].startDate.getHours(); this.sMinute = event[0].startDate.getMinutes();
          this.eHour = event[0].endDate.getHours(); this.eMinute = event[0].endDate.getMinutes()

          if(this.sHour <10){
            this.sHour = "0" + this.sHour;
          }
          if(this.sMinute<10){
            this.sMinute = "0" + this.sMinute;
          }
          this.sTime = this.sHour + ":" + this.sMinute;

          if(this.eHour <10){
            this.eHour = "0" + this.eHour;
          }
          if(this.eMinute<10){
            this.eMinute = "0" + this.eMinute;
          }
          this.eTime = this.eHour + ":" + this.eMinute;
        });

      this.appService.getVols(this.id)
        .subscribe(vols => {this.eventVols = vols;})

//        this.dt = this.event[0].startDate;
  }

  toggleEditMode(event){
    if(this.editMode){
      this.editMode=false;
      this.editText='Edit Mode';
    }
    else{
      this.editMode=true;
      this.editText='Exit Edit Mode';
    }
  }

signUp(){
  let eventVolunteer = new EventVolunteer(this.name,this.email,this.id);
  console.log(JSON.stringify(eventVolunteer));
  this.appService.register(eventVolunteer)
    .subscribe(data => console.log('Sign up completed: '+ JSON.stringify(data)));
}

  createNewEvent(){
    var addr = this.event[0].street1 + ',' + this.event[0].street2 + ',' + this.event[0].city + ',' + this.event[0].state + ',' + this.event[0].zipCode;
    addr = addr.replace(/\s+/g, '+');
    var dateArray = this.dt.split("-");
    var startTimeArray = this.sTime.split(":");
    var endTimeArray = this.eTime.split(":");
    this.event[0].startDate = new Date(dateArray[0], dateArray[1]-1, dateArray[2], startTimeArray[0], startTimeArray[1], 0, 0);
    this.event[0].endDate = new Date(dateArray[0], dateArray[1]-1, dateArray[2], endTimeArray[0], endTimeArray[1], 0, 0);


    this.appService.getCoordinates(addr)
      .subscribe(data=> {
        console.log('addr: ' + JSON.stringify(data));
        this.event[0].latitude=data.lat;
        this.event[0].longitude=data.lng;

        this.appService.updateEvent(this.event[0])
          .subscribe(data => console.log('Event Updated: '+ JSON.stringify(data)));

        this.editMode = false;
        this.editText='Edit Mode';
      });
    console.log("Updated event: " + JSON.stringify(this.event[0]));

  }

  goBack(): void {
    this.location.back();
  }

  closed() {
      this.signUp();
      console.log('closed;');
      this.location.back();
  }

  dismissed() {
      console.log('dismissed;');
  }

  opened() {
      console.log('opened;');
  }

}
