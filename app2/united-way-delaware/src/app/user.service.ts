import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {

  loggedIn: boolean;

  constructor(private http: Http) {this.loggedIn = false; }

  setLoggedIn(value){
    this.loggedIn = value;
  }

}
