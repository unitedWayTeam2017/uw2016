import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Event, EventCategory, VolEvent } from './event';


@Injectable()
export class VolunteerService {
  public testCategory: EventCategory = new EventCategory(1, 'Personal "Donation"', 'Mockup');

  public events: Event[] = [ new Event(1, 'Women United Networking and Art Exhibit', '03/16/2017', '8:00AM', '9:00PM', '', '5517 Limeric Circle', 'apt 24', 'Wilmington', 'DE', '19808', -75.5498539, 39.7466083, this.testCategory),
    new Event(2, 'Sneaker Ball', '04/08/2017', '8:00AM', '9:00PM', '','343 Norman Drive', '', 'Newark', 'DE', '19702', -75.727046, 39.624245, this.testCategory),
    new Event(3, 'MVOL Celebration', '05/23/2017', '10:00AM', '1:00PM', '', '20 Orchard Rd', '', 'Newark', 'DE', '19716', -75.7567428, 39.6806881, this.testCategory)];

  private productUrl = 'https://jsonplaceholder.typicode.com/posts/1';
  res: string;
  vols: VolEvent[];
  /*
    TODO: HTTP is going to be used for fetching data
  */

  constructor(private http: Http) {
  //  console.log(
      this.http.get('http://uwde-rest.herokuapp.com/eventCategory')
        .map(function(res){
          //console.log('map-'+JSON.stringify(res.json()));
          let body = <VolEvent[]>res.json();
          //console.log(body);
          return body;})
        .subscribe(
          function(result){
            //console.log('resulttttt ' + JSON.stringify(result));
          //  console.log(result);
            this.vols = result;

            //console.log(this.vols);
            this.res = JSON.stringify(result);
          });
//  );
//console.log(this.vols);
//      console.log('result ' + this.res);
   }

  testService(): Observable<VolEvent[]> {
    return this.http.get('http://uwde-rest.herokuapp.com/events')
      .map((response: Response) => <VolEvent[]>response.json())
    //  .do(data => console.log('Test: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  public getVolunteers(): void {
    var testCategory: EventCategory = new EventCategory(1, 'Personal "Donation"', 'Mockup');
  }

  public getEvents(): Promise<Event[]> {
    return Promise.resolve(this.events);
  }

  public getEvent(id: number): Promise<Event> {
    //return this.getEvents().find(event => event.id === id);
    return this.getEvents()
             .then(events => events.find(event => event.id === id));
  }

  public getVols(): Observable<any> {
    return this.http.get("http://localhost:8080/events/").map((response:Response) => {
        let body = response.json();
        return body.data;
    }).catch((error: any, cought: Observable<any>) => {
      console.log(error);
      return null;
    });
  }

  public getVolunteer(id: number): Event {
    var eventsCurr = this.getEvents()
             .then(events => this.events = events);
    let eventList: Event[] = this.events;
    for (var i = 0; i < eventList.length; i++) {
      if(id == eventList[i].id) return eventList[i];
    }

    return null;
  }

}
