import { Component, OnInit  } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import { ActivatedRoute, ActivatedRouteSnapshot, Router, NavigationEnd } from '@angular/router';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ AppService ]
})
export class AppComponent implements OnInit {
  public title: string;
  static login = false;

  public constructor(private appService: AppService,private titleService: Title, private router:Router, private activeRoute:ActivatedRoute ) { }
  public setTitle( newTitle: string) {
    this.title = newTitle;
    this.titleService.setTitle( newTitle );
  }

  private getDeepestTitle(routeSnapshot: ActivatedRouteSnapshot) {
    var title = routeSnapshot.data ? routeSnapshot.data['title'] : '';
    console.log(routeSnapshot);
    if (routeSnapshot.firstChild) {
      title = this.getDeepestTitle(routeSnapshot.firstChild) || title;
    }
    return title;
  }

  public static setLogin(flag) {
  console.log(flag);
    AppComponent.login = flag;
    console.log(AppComponent.login);
  }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if(event instanceof NavigationEnd)
        this.setTitle(this.getDeepestTitle(this.router.routerState.snapshot.root));
    });
    AppComponent.login = this.appService.isLogIn();
    console.log(AppComponent.login);
  }
}
