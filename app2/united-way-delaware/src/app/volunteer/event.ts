export class Event {
    constructor(/*public id: number,*/
                public name: String,
                public startDate: Date,
                public endDate: Date,
                public descr: string,
                public street1: string,
                public street2: string,
                public city: string,
                public state: string,
                public zipCode: string,
                public latitude: string,
                public longitude: string) { }
}

export class EventVolunteer {
    constructor(public name: string,
                public email: string,
                public eventId: number) { }
}
