/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MywayComponent } from './myway.component';

describe('MywayComponent', () => {
  let component: MywayComponent;
  let fixture: ComponentFixture<MywayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MywayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MywayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
