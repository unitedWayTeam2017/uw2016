import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../app.service';
import { DonationCategory, Donation } from './give';

@Component({
  selector: 'app-give',
  templateUrl: './give.component.html',
  styleUrls: ['./give.component.css'],
  providers: [AppService]
})
export class GiveComponent implements OnInit {
  @ViewChild('select') selectElRef;
  @ViewChild('modal')
  animation: boolean = true;
  keyboard: boolean = true;
  backdrop: string | boolean = true;
  cssClass: string = '';

  confirmation: boolean = false;

  selectedValues = [];

  amounts = [10,22,52];
  amountInvalid = false;
  amount = 10;
  minAmount = 10;

  sliderValue = 200;
  name = 'Admin';
  email = 'admin@admin.com';

  donationCategories: DonationCategory[];

  constructor(private appService: AppService) { }

  ngOnInit() {
    this.appService.getDonationCategories()
      .subscribe(data => this.donationCategories = data);
  }

  sliderChange(value) {
    this.sliderValue = value;
    console.log('Current Slider value: ' + value);
  }

  change(options) {
    this.selectedValues = Array.apply(null,options)  // convert to real Array
      .filter(option => option.selected)
      .map(option => option.value)
      console.log('Selected Categories: ' + this.selectedValues);
  }

  changeAmount(n) {
    this.amount = n;
    this.amountInvalid = false;
  }

  validateAmount(){
  this.amountInvalid = false;

    if (this.amount < this.minAmount) {

      this.amountInvalid = true;
      console.log('Invalid amount:' + this.amount);
    }
  }

  onSubmit() {
    console.log('Submitting ... ');
//    let donation = new Donation('A','B','C',123.45);
    let donation = new Donation(this.name,this.email,this.selectedValues.join(','),this.amount);
    console.log(JSON.stringify(donation));
    this.appService.saveDonation(donation)
      .subscribe(data => console.log('Donation completed: '+ JSON.stringify(data)));
    this.confirmation = true;
  }

  reset() {
    this.confirmation = false;
    this.sliderValue = 200;
    this.name = 'Admin';
    this.email = 'admin@admin.com';
    this.selectedValues = [];
  }

  closed() {
      this.onSubmit();
      console.log('closed;');
  }

  dismissed() {
      console.log('dismissed;');
  }

  opened() {
      console.log('opened;');
  }
}
