export class User {
	public name: string;
	public email: string;
	public address: string;
	public password: string;

    constructor() { }

    setName(newName:string){
    	this.name = newName;
    }

    setEmail(newEmail:string){
    	this.email = newEmail;
    }

    setAddress(newAddress:string){
    	this.address = newAddress;
    }

    setPassword(newPassword:string){
    	this.password = newPassword;
    }
}