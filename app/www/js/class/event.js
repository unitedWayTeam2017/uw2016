var Event = (function() {
	function Event(id, name, date, startTime, endTime, description, location, category){
		this.id = (id == undefined) ? 0 : id;
		this.name = (name == undefined) ? null : name;
		this.date = (date == undefined) ? null : date;
		this.startTime = (startTime == undefined) ? null : startTime;
		this.endTime = (endTime == undefined) ? null : endTime;
		this.description = (description == undefined) ? "" : description;
		this.location = (location == undefined) ? null : location;
		this.category = (category == undefined) ? null : category;
	};
	Event.prototype.validate = function(){
		/* TODO */
		return true;
	};
	return Event;
}());

var EventLocation = (function(){
	function EventLocation(id, address, city, state, zipcode, longitude, latitude){
		this.id = (id == undefined) ? 0 : id;
		this.address = (name == undefined) ? null : address;
		this.city = (city == undefined) ? null : city;
		this.state = (state == undefined) ? null : state;
		this.zipcode = (zipcode == undefined) ? null : zipcode;
		this.longitude = (longitude == undefined) ? null : longitude;
		this.latitude = (latitude == undefined) ? null : latitude;
	};
	EventLocation.prototype.validate = function(){
		/* TODO */
		return true;
	};
	EventLocation.prototype.getPosition = function() {
		return new google.maps.LatLng(this.latitude, this.longitude);
	}
	return EventLocation;
}());

var EventCategory = (function(){
	function EventCategory(id, name, description){
		this.id = (id == undefined) ? 0 : id;
		this.name = (name == undefined) ? null : name;
		this.description = (description == undefined) ? null : description;
	};
	EventCategory.prototype.validate = function(){
		/* TODO */
		return true;
	};
	return EventCategory;
}());
