
var volMapCtrl = (function(){
	var dependency = ['$rootScope', '$scope', '$http']; 
    var map;
    var bounds;

    function initializeMap(events) {
        bounds = new google.maps.LatLngBounds();
        var mapOptions = {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom:8
        }

        map = new google.maps.Map(document.getElementById('volunteer-map'), mapOptions);

        var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
            this.setZoom(14);
            google.maps.event.removeListener(boundsListener);
        });
        for( i = 0 ; i < events.length; i++ ) {
            var event = events[i];
            bounds.extend(event.location.getPosition());
            var marker = new google.maps.Marker({ position:event.location.getPosition(), title: event.name, map:map});

            map.fitBounds(bounds);
        }
    }
    function addMarker(event) {
        bounds.extend(event.getPosition());
        var marker = new google.maps.Marker({ position: event.location.getPosition(), map: map, title: event.name });
        map.fitBounds(bounds);
    }

	function controller(rs, $scope, $http) {
		rs.pageTitle = "Volunteer Map";
        initializeMap($scope.events);
	};

	dependency.push(controller);
	return dependency;
}());
