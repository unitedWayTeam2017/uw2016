package mappers;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.uwde.dao.User;

public interface UserMapper {

	@Select("SELECT * FROM Users")
    List<User> getAllUsers();

    @Select("SELECT * FROM Users WHERE id = #{id}")
	List<User> getUserById(int id);

	
}
