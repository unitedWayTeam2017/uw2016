package mappers;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.uwde.dao.User;

public interface LoginMapper {	

		@Select("SELECT * FROM Users WHERE email = #{desiredName}")
	    User emailTaken(String desiredName);

		@Insert("Insert into Users (name, email, address, password) values "
				+ "(#{name}, #{email}, #{address}, #{password})")
		void addNewUser(User user);

		@Select("select count(*) from Users where email = #{email} and password = #{password}")
		int login(User user);

}
