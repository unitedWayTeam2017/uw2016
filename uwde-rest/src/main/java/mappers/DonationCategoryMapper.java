package mappers;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.uwde.dao.DonationCategory;

@Mapper
public interface DonationCategoryMapper {

	@Select("SELECT * FROM donation_category")
  List<DonationCategory> getAllDonationCategories();

  @Select("SELECT * FROM donation_category WHERE id = #{id}")
	List<DonationCategory> getDonationCategoryById(int id);

}
