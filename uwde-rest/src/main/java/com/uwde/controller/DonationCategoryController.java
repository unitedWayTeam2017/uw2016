package com.uwde.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.uwde.dao.DonationCategory;

import mappers.DonationCategoryMapper;

@RestController
@RequestMapping("/donationCategory")
public class DonationCategoryController {

	@Autowired
	DonationCategoryMapper donationCategoryMapper;

	@RequestMapping(method = {RequestMethod.GET})
    public List<DonationCategory> getAllDonationCategories() {
		return donationCategoryMapper.getAllDonationCategories();
    }

	@RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    public List<DonationCategory> getDonationCategoryById(@PathVariable int id) {
		return donationCategoryMapper.getDonationCategoryById(id);
    }

}
