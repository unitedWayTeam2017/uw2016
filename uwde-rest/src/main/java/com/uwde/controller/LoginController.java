package com.uwde.controller;

import javax.xml.ws.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.uwde.dao.User;

import mappers.LoginMapper;

@CrossOrigin
@RestController
@RequestMapping("/login")
public class LoginController {

	@Autowired
	LoginMapper loginMapper;
	
	@CrossOrigin
	@RequestMapping(value = "/emailTaken", method = {RequestMethod.GET})
	@ResponseBody
	public User emailTaken(@RequestParam String desiredName){
		User u = loginMapper.emailTaken(desiredName);
		return u;		
	}
	
	@CrossOrigin
	@RequestMapping(value = "/addNewUser", method = {RequestMethod.POST})
	@ResponseBody
	public User addNewUser(@RequestBody User user){
		loginMapper.addNewUser(user);
		return user;	
	}
	
	@CrossOrigin
	@RequestMapping(value = "/login", method = {RequestMethod.POST})
	@ResponseBody
	public boolean login(@RequestBody User user){
		int valid = loginMapper.login(user);
		if (valid != 0){
			return true;
		}
		else{
			return false;
		}
	}
}
