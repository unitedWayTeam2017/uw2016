package com.uwde.controller;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mappers.EventMapper;

@RestController
public class VolunteerController {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    @Autowired
    EventMapper mapper;
    
    @RequestMapping("/getVolunteer")
    public List<com.uwde.dao.Event> greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return mapper.getAllEvents();
    }
}
