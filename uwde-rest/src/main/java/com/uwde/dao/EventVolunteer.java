package com.uwde.dao;
import java.sql.Timestamp;

public class EventVolunteer {

	private int id;
	private String name;
	private String email;
  private int eventId;
	private Timestamp date;

	public EventVolunteer(){}
  public EventVolunteer(String name, String email, int eventId) {
    this.name = name;
    this.email = email;
    this.eventId = eventId;
  }
	public String toString() {
		return name + " : " + email;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

  public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}
