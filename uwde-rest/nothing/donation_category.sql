create table donation_category (
	id SERIAL primary key,
	name varchar(255) not null,
	description varchar(255) not null
);

insert into donation_category (name, description) values
('United Way General', 'Description - United Way General'),
('Early Education Success', 'Description - Early Education Success'),
('College and Career Readiness', 'Description - College and Career Readiness');
