create table donation (
	id SERIAL primary key,
	name varchar(255) not null,
	email varchar(255) not null,
  amount float(2) not null,
  category varchar(1024)
);
